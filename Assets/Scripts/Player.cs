﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	[HideInInspector]
	public float QRedyts;
	[HideInInspector]
	public float Multiplier;
	//[HideInInspector]
	//public List<GameObject> Billboards = new List<GameObject>();
	public GameObject Canvas1, Canvas2;
	[HideInInspector]
	public GameObject CurrentBillboard;
	[HideInInspector]
	public Vector3 PlayerLocation;
	[HideInInspector]
	public bool ChosenADifficulty;

	[SerializeField]
	private Text qredytsDisplay;
	[SerializeField]
	private Text timerDisplay;

	[SerializeField]
	private float timer;

	private int playerIDNum;

	void Awake () {
		ChosenADifficulty = false;
		timer = PlayerPrefs.GetFloat ("Timer");

		if (PlayerPrefs.GetFloat ("Multiplier") > 0) {
			Multiplier = PlayerPrefs.GetFloat ("Multiplier");
		} else {
			Multiplier = 1;
		}
	}

	void Update () {
		qredytsDisplay.text = QRedyts.ToString();
		timer -= Time.deltaTime;
		if (timer > 0) {
			timerDisplay.text = timer.ToString ("F1");
		}else if (timer <= 0) {
			Multiplier = 1;
			timerDisplay.text = "No Boost";
		}
	}

	public void DifficultySetToEasy() {
		CurrentBillboard.GetComponent<BillboardQuestion> ().difficulty = BillboardQuestion.Difficulty.Easy;
		ChoseADifficulty ();
	}
	public void DifficultySetToModerate() {
		CurrentBillboard.GetComponent<BillboardQuestion> ().difficulty = BillboardQuestion.Difficulty.Moderate;
		ChoseADifficulty ();
	}
	public void DifficultySetToHard() {
		CurrentBillboard.GetComponent<BillboardQuestion> ().difficulty = BillboardQuestion.Difficulty.Hard;
		ChoseADifficulty ();
	}

	public void ChoseADifficulty(){
		Canvas1.SetActive(false);
		Canvas2.SetActive(true);
		ChosenADifficulty = true;
	}

	public void UserSelectTrue(){
		if (CurrentBillboard.GetComponent<BillboardQuestion> ().CurrentQuestion.isTrue) {
			Debug.Log ("Correct Answer");
			Canvas2.SetActive (false);
			QRedyts += 1 * Multiplier;
			Debug.Log (QRedyts);
		} else {
			Debug.Log ("Wrong Answer");
			Canvas2.SetActive (false);
		}
	}

	public void UserSelectFalse(){
		if (!CurrentBillboard.GetComponent<BillboardQuestion> ().CurrentQuestion.isTrue) {
			Debug.Log ("Correct Answer");
			Canvas2.SetActive (false);
			QRedyts += 1 * Multiplier;
			Debug.Log (QRedyts);
		} else {
			Debug.Log ("Wrong Answer");
			Canvas2.SetActive (false);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Billboard") {
			CurrentBillboard = other.gameObject;
			Canvas1.SetActive(true);
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Billboard") {
			CurrentBillboard = null;
			Canvas1.SetActive(false);
			Canvas2.SetActive(false);
			ChosenADifficulty = false;
		}
	}
}
