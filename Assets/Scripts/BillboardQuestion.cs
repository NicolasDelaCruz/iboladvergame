﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BillboardQuestion : MonoBehaviour {

	[HideInInspector]
	public enum Difficulty{Easy, Moderate, Hard}; public Difficulty difficulty;
	public Query[] Questions;	
	public Query CurrentQuestion;

	private static List<Query> unansweredQuestions;

	[SerializeField]
	private Text questionText;

	void Start () {
		if (unansweredQuestions == null || unansweredQuestions.Count == 0) {
			unansweredQuestions = Questions.ToList<Query> ();
		}
	}

	void OnTriggerStay2D(Collider2D other) {
		
		if (other.tag == "Player" && other.GetComponent<Player> ().ChosenADifficulty == true) {
			int randomQuestionIndex = Random.Range (0, unansweredQuestions.Count);
			CurrentQuestion = unansweredQuestions [randomQuestionIndex];


			questionText.text = CurrentQuestion.Question;
			other.GetComponent<Player> ().ChosenADifficulty = false;

			unansweredQuestions.RemoveAt (randomQuestionIndex);
		}
	}
}
