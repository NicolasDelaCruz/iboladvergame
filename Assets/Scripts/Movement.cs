﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public float MovementSpeed;

	void Update () {
		if (Input.GetKey (KeyCode.A)) {
			this.transform.Translate (new Vector3 (-MovementSpeed * Time.deltaTime, 0));
		}
		if (Input.GetKey (KeyCode.D)) {
			this.transform.Translate (new Vector3 (MovementSpeed * Time.deltaTime, 0));
		}
		if (Input.GetKey (KeyCode.S)) {
			this.transform.Translate (new Vector3 (0, -MovementSpeed * Time.deltaTime));
		}
		if (Input.GetKey (KeyCode.W)) {
			this.transform.Translate (new Vector3 (0, MovementSpeed * Time.deltaTime));
		}
	}
}
