﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multiplier : MonoBehaviour {

	private float DefaultMultiplier = 1;

	public void ChangeMultipierTo(float multiplyBy){
		if (PlayerPrefs.GetFloat ("Multiplier") != multiplyBy) {
			PlayerPrefs.SetFloat ("Multiplier", multiplyBy);
			PlayerPrefs.SetFloat ("Timer", 30.0f);
		} else if (PlayerPrefs.GetFloat ("Multiplier") == multiplyBy) {
			PlayerPrefs.SetFloat ("Multiplier", DefaultMultiplier);
			PlayerPrefs.SetFloat ("Timer", 0.0f);
		}
	}
}
