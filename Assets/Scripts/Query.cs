﻿[System.Serializable]
public class Query{

	public string Question;
	public bool isTrue;

	public enum Difficulty {
		Easy, Moderate, Hard
	};
	public Difficulty difficulty;
}
