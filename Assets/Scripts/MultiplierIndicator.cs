﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiplierIndicator : MonoBehaviour {

	[SerializeField]
	private Sprite[] multiplierSprite;
	[SerializeField]
	private GameObject player;

	void Update () {
		if (player.GetComponent<Player>().Multiplier < 2){
			gameObject.GetComponent<Image> ().sprite = multiplierSprite [0];
		}
		if (player.GetComponent<Player>().Multiplier == 2){
			gameObject.GetComponent<Image> ().sprite = multiplierSprite [1];
		}
		if (player.GetComponent<Player>().Multiplier == 4){
			gameObject.GetComponent<Image> ().sprite = multiplierSprite [2];
		}
		if (player.GetComponent<Player>().Multiplier == 8){
			gameObject.GetComponent<Image> ().sprite = multiplierSprite [3];
		}
	}
}
