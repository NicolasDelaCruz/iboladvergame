﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene : MonoBehaviour {

	public string LoadSceneName;

	public void Level () {
		SceneManager.LoadScene(LoadSceneName);
	}
}
