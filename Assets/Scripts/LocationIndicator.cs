﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocationIndicator : MonoBehaviour {

	[SerializeField]
	private string currentLocation;
	[SerializeField]
	private Text locationText;

	void OnTriggerStay2D(Collider2D other){
		if (other.tag == "Player") {
			locationText.text = currentLocation;
		}
	}
}
