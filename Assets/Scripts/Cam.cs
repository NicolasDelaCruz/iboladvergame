﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam : MonoBehaviour {

	public WebCamTexture camera;
	public GameObject plane;

	IEnumerator Start () {
		yield return Application.RequestUserAuthorization (UserAuthorization.WebCam);
		plane = GameObject.FindWithTag ("MobCam");
		camera = new WebCamTexture ();
		plane.GetComponent<Renderer> ().material.mainTexture = camera;
		camera.Play ();
	}
}
